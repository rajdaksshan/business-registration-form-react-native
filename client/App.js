import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet, 
  Alert,
   Image,
} from 'react-native';
import { CheckBox, ScrollView, Picker } from 'react-native';
import {
  Appbar,
  RadioButton,
  TextInput,
  Button,Card
} from 'react-native-paper';
class Inputs extends Component {
  state = {
    name: '',
    age: '',
    bname: '',
    email: '',
    address: '',
    password: '',
    phonenumber: '',
    state1: '',
    value1: '',
    checked: false,
    isSwitchOn: false,
    choosenIndex: 0,
    shoptype: ''
  };
  onClick = () => {
    fetch('http://192.168.72.44:5000/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: this.state.name,
        age: this.state.age,
        bname: this.state.bname,
        username: this.state.email,
        password: this.state.password,
        mobnumber: this.state.phonenumber,
        Gender: this.state.value1,
        Address: this.state.address,
        BusinessType: this.state.shoptype,

      }),
    }).then(() => Alert.alert("Business is registered Successfully"))
      .catch(err => console.warn(err));
  }
  handleName = text => {
    this.setState({ name: text });
  };
  handlePhonenumber = text => {
    this.setState({ phonenumber: text });
  };
  handleAge = text => {
    this.setState({ age: text });
  };

  handleBname = text => {
    this.setState({ bname: text });
  };

  handleEmail = text => {
    this.setState({ email: text });
  };
  handlePassword = text => {
    this.setState({ password: text });
  };
  handleState1 = text => {
    this.setState({ state1: text });
  };
  handleAddress = text => {
    this.setState({ address: text });
  };
  handlePhonenumber = text => {
    this.setState({ phonenumber: text });
  };

  render() {
    const { value1 } = this.state;
    return (
      
      <ScrollView >
       <Appbar.Header style={styles.head}>
          <Appbar.Content title="Business Registration Form" />
        </Appbar.Header>
         <Image style={styles.image}
          source={require('/components/10.jpg')}
        />
          <Card >
            <Card.Content>

              <TextInput mode='outlined'
                label="Owner Name"
                value={this.state.name}
                onChangeText={this.handleName}
              />
              <View style={{ marginTop: 10 }}>
                <Text style={{ marginTop: 10 }}>Gender</Text>
                <View style={{ flexDirection: 'row' }}>
                  <RadioButton.Group
                    onValueChange={value => this.setState({ value1: value })}
                    value={value1}>
                    <RadioButton value="male" color='black' />
                    <Text style={{ marginTop: 10 }} >Male</Text>
                    <RadioButton value="female" color='black' />
                    <Text style={{ marginTop: 10 }}>Female</Text>
                  </RadioButton.Group>
                </View>
              </View>

              <TextInput mode='outlined'
                label="Age"
                value={this.state.age}
                onChangeText={this.handleAge}
              />
              <TextInput mode='outlined'
                label="Email"
                autoCapitalize="none"
                value={this.state.email}
                onChangeText={this.handleEmail}
              />
              <TextInput mode='outlined'
                label="Password"
                value={this.state.password}
                secureTextEntry={true}
                onChangeText={this.handlePassword}
              />
              <TextInput mode='outlined'
                label="Business Name"
                value={this.state.bname}
                onChangeText={this.handleBname}
              />

              <TextInput mode='outlined'
                label="Phone Number"
                keyboardType={'number-pad'}
                value={this.state.phonenumber}
                onChangeText={this.handlePhonenumber}
              />
              <TextInput mode='outlined'
                label="Address"
                value={this.state.address}
                onChangeText={this.handleAddress}
              />
              <TextInput mode='outlined'
                label="State"
                value={this.state.state1}
                onChangeText={this.handleState1}
              />
              <Text style={{ marginTop: 10 }}>Business Type</Text>
              <Picker mode='outlined'
                label="Type of Business"
                style={styles.pickerStyle}
                selectedValue={this.state.shoptype}
                onValueChange={(itemValue, itemPosition) =>
                  this.setState({
                    shoptype: itemValue,
                    choosenIndex: itemPosition,
                  })
                }>
                <Picker.Item label="None" value="None" />
                <Picker.Item label="Shop/Cafe" value="Shop/Cafe" />
                <Picker.Item label="Education" value="Education" />
                <Picker.Item label="Store" value="Store" />
                <Picker.Item label="Others" value="Others" />
              </Picker>

              <View style={{ flexDirection: 'row' }}>
                <CheckBox
                  style={{ marginTop: 5 }}
                  value={this.state.isSwitchOn}
                  onValueChange={() =>
                    this.setState({ isSwitchOn: !this.state.isSwitchOn })
                  }
                />
                <Text style={{ marginTop: 10 }}>
                  I agree the terms & condition
              </Text>
              </View>
              <View style={styles.buttonview} >
                <Card>
                <View style={{flex:10}}>
                  <Button mode="contained"
                    style={styles.submitButton}
                    onPress={this.onClick}>
                    Submit
              </Button>
              </View>
                </Card>
              </View>
            </Card.Content>
          </Card>
      </ScrollView>
  
    );
  }
}
export default Inputs;
const styles = StyleSheet.create({
  head: {
    backgroundColor: 'black',
    textAlign: 'center',
  },
  buttonview: {
    alignItems: 'center',
  },
  image:{
    flex:1,
   width: 300,
   justifyContent:'center',
   height: 350,
  },
  submitButton: {
    backgroundColor: 'black',
    alignItems: 'center',
    textAlign: 'center',
    width: 150,
  },

});
